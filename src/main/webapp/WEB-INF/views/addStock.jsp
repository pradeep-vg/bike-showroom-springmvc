<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <title>Product</title>
    <link href="../../assets/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="../../assets/css/mainForm.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="../../assets/css/bootstrap.css">

</head>
<body>
<%@include file="menu.jsp" %>
<div class="page-wrapper bg-gra-01 p-t-180 p-b-100 font-poppins">
    <div class="wrapper wrapper--w780">
        <div class="card card-3" style="width: max-content;margin-left: 25%;">
            <div class="card-body" >
                <c:if test="${error!=null}">
                    <div class="title">
                <c:set var="error" scope="request" value="${error}"/>
                <c:out value="${error}"/>
                    </div>
                </c:if>
                <c:choose>
                    <c:when test="${stock.id!=0}">
                        <h2 class="title">Update Stock</h2>
                    </c:when>
                    <c:when test="${stock.id==0}">
                        <h2 class="title">Stock Entry</h2>
                    </c:when>
                </c:choose>
                <form:form method="post" modelAttribute="stock" action="/stock">
                    <label style="color: rgb(55,203,89); font-size: 20px"><b>Product name :</b></label>
                    <a href="/product/productDetails/${product.id}" class="btn btn-primary" >${product.productName}</a>
                    <form:input path="product.id" type="hidden" value="${product.id}"/><br/>
                    <label style="color: rgb(55,203,89); font-size: 20px"><b>Color :</b></label>
                    <form:select path="color.id" required="required" Class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                        <c:forEach var="color" items="${color}" varStatus="counter">
                        <option class="dropdown-item" value="${color.id}"><c:out value="${color.color}"/></option>
                        </c:forEach>
                    </form:select><br/>
                    <tr><td>
                    <label style="color: rgb(55,203,89); font-size: 20px"><b>No of bikes :</b></label></td>
                        <td>
                    <form:input type="text"  class="form-control"  placeholder="Stock" name="stock" path="quantity" required="required" />
                        </td>
                    </tr>
                    <button  class="btn btn--pill btn--green border" style="margin-left: 25%" type="submit">Add Stock</button>
                        <%--<c:choose>
                        <c:when test="${stock.id!=0}">
                            <button class="btn btn--pill btn--green" type="submit">Update Stock</button>
                        </c:when>
                        <c:when test="${stock.id==0}">
                            <button  class="btn btn--pill btn--green" style="margin-left: 25%" type="submit">Add Stock</button>
                        </c:when>
                        <c:otherwise>
                            "Something Error"
                        </c:otherwise>
                    </c:choose>--%>
                </form:form>
            </div> </div></div></div>
</body>
</html>
