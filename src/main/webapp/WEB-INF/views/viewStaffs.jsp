<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <%-- <script src="../../assets/vendor/bootstrap/js"></script>
     <script src="../../assets/vendor/jquery"></script>--%>
    <%--<link rel="stylesheet" href="../../assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../assets/css/all.css">--%>
    <title>Staffs</title>

</head>
<body class="bg">
<%@include file="menu.jsp" %>
<h1 class="site-heading-upper text-primary mb-3" style="margin-left: 50%">STAFFS</h1>
${error}
<a class="btn btn-outline-light" style="margin-left:85%" href="/user/createPage">Add Staff</a>

<div class="card text-white bg-secondary mb-3">
    <div class="card-header"><i class="fas fa-table mr-1"></i>List of staffs</div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-dark" id="dataTable" width="100%" cellspacing="0">
                <tr>
                    <th>S.NO</th>
                    <th hidden>ID</th>
                    <th>FirstName</th>
                    <th>LastName</th>
                    <th>Mobile</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Username</th>
                    <th>Password</th>

                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="user" items="${users}" varStatus="counter">
                    <tr>
                        <td><c:out value="${counter.count}"/></td>
                        <td hidden><c:out value="${user.id}"/></td>
                        <td><c:out value="${user.firstName}"/></td>
                        <td><c:out value="${user.lastName}"/></td>
                        <td><c:out value="${user.mobile}"/></td>
                        <td><c:out value="${user.address}"/></td>
                        <td><c:out value="${user.email}"/></td>
                        <td><c:out value="${user.username}"/></td>
                        <td><c:out value="${user.password}"/></td>

                        <td>
                            <a class="btn btn-sm btn-outline-warning" href="updateUser/${user.id}"><i
                                    class="fas fa-user-edit"></i></a>
                            &nbsp;
                            <a class="btn btn-sm btn-outline-danger" href="deleteUser/${user.id}"><i
                                    class="far fa-trash-alt"></i></a>

                        </td>
                    </tr>
                </c:forEach>

                </tbody>

            </table>
        </div>
    </div>
</div>

<script src="../../assets/vendor/bootstrap/js/all.js"></script>
<script src="../../assets/vendor/bootstrap/js/"></script>
</body>
</html>
