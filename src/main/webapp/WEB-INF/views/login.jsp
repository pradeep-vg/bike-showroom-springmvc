<html>
<head>
  <%@ page contentType="text/html;charset=UTF-8" language="java" %>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <link rel="stylesheet" type="text/css" href="../../assets/css/login.css">

  <title>Login Page</title>
</head>
<body id="LoginForm">
<h1 style="margin-left: 30%" display="red" class="card-title">WELCOME TO YAMAHA SHOWROOM</h1>
<div class="container">
  <div class="login-form">
    <div class="main-div">
      <div class="panel">
        <c:set var="error" scope="request" value="${error}"/>
        <c:out value="${error}"/>
        <h2 > Login</h2>
      </div>
      <form  action="/session" method="post">

        <div class="form-group">


          <input type="text" class="form-control" name="username" placeholder="Username" required>
        </div>

        <div class="form-group">

          <input type="password" class="form-control" name="password" placeholder="Password" required>

        </div>

        <button type="submit"  class="btn btn-primary">Login</button>

      </form>
    </div>
  </div></div></div>


</body>
</html>