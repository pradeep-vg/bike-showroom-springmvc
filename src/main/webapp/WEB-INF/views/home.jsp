<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Home</title>

    <!-- Bootstrap core CSS -->
    <link href="../../assets/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="../../assets/vendor/bootstrap/js/bootstrap.bundle.js" >
    <link rel="stylesheet" href="../../assets/vendor/jquery/jquery.slim.js" >
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


</head>
<body class="bg">
<c:choose><c:when test="${sessionScope.currentUser!=null}">
    <%@include file="menu.jsp" %>
    <h1 class="site-heading text-center text-white d-none d-lg-block">
        <span class="site-heading-upper text-primary mb-3">WELCOME TO </span>
        <span class="site-heading-lower">YAHAMA SHOWROOM</span>
    </h1>






    <h1 class="head" style="margin-left:40%; color: red">Welcome <c:out
            value="${sessionScope.currentUser.firstName}"/> !!</h1>
</c:when>
    <c:otherwise>
        <h1 class="head" style="margin-left:40%; color: bisque">Invalid User Login Again!!</h1>
        <br>
        <a style="margin-left: 50%" class="btn btn-danger" href="/">Login</a>
    </c:otherwise>
</c:choose>
<c:if test="${error!=null}">
    <div class="title">
    <c:set var="error" scope="request" value="${error}"/>
    <c:out value="${error}"/>
    </div>
</c:if>
</body>
</html>
