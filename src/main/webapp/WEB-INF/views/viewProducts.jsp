<html>
<head>
    <title>Products</title>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

</head>
<body class="bg">
<%@include file="menu.jsp" %>
<h1 class="site-heading-upper text-primary mb-3" style="margin-left: 40%">Available Bikes</h1>
${error}
<c:forEach var="role" items="${sessionScope.currentUser.roles}" varStatus="counter">
<c:if test="${(counter.count==1) && (role.name.equalsIgnoreCase('admin')||role.name.equalsIgnoreCase('staff'))}">
<a class="btn btn-outline-light" href="/product/productForm" style="margin-left:77%">Add Bike</a>
</c:if>
</c:forEach>
<div class="card text-white bg-secondary mb-3">
    <div class="card-header"><i class="fas fa-table mr-1"></i>List of staffs</div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-dark" id="dataTable" width="100%" cellspacing="0">
                <thead>
    <tr>
        <th>S.NO</th>
        <th>Bike Name</th>
        <th>Unit Price</th>
       <%-- <th>Stock</th>--%>
        <th>CC</th>
        <th>Mileage</th>
        <th>Weight</th>
      <%--  <th>Updated on</th>--%>
        <th>Details</th>
        <c:forEach var="role" items="${sessionScope.currentUser.roles}" varStatus="counter">
            <c:if test="${(counter.count==1) && (role.name.equalsIgnoreCase('admin')||role.name.equalsIgnoreCase('staff'))}">
        <th>Action</th>
    <th>Sell Bike</th>
</c:if>
</c:forEach>

    </tr>
    </thead>
    <tbody>

    <c:forEach var="product" items="${products}" varStatus="counter">

        <tr>
            <td> ${counter.count}</td>
            <td hidden><c:out value="${product.id}"/></td>
            <td><c:out value="${product.productName}"/></td>
            <td><c:out value="${product.unitPrice}"/></td>
           <%-- <td><c:out value="${product.quantity}"/></td>--%>
            <td><c:out value="${product.cc}"/></td>
            <td><c:out value="${product.mileage}"/></td>
            <td><c:out value="${product.weight}"/></td>

          <%--  <td><c:out default="N/A" value="${product.updatedBy.username!='' ?product.updatedBy.username : 'N/A'}"/></td>

            <td><c:out default="N/A" value="${product.lastUpdate!= null ?product.lastUpdate : 'N/A'}"/></td>
            --%>
            <td>
                <a class="btn btn-sm btn-success" href="/product/productDetails/${product.id}">View Details</a>
            </td>
            <c:forEach var="role" items="${sessionScope.currentUser.roles}" varStatus="counter">
                <c:if test="${(counter.count==1) && (role.name.equalsIgnoreCase('admin')||role.name.equalsIgnoreCase('staff'))}">
            <td>
                <a class="btn btn-sm btn-success" href="/stock/${product.id}">Add Stock</a>

                <a class="btn btn-sm btn-outline-warning" href="/product/update/${product.id}"><i class="fas fa-edit " ></i></a>
                &nbsp;
                <a class="btn btn-sm btn-outline-danger" href="/product/delete/${product.id}"><i
                        class="far fa-trash-alt "></i></a>

            </td>
            </c:if>
        </c:forEach>

            <td>
                <a class="btn btn-sm btn-danger" href="/sales/${product.id}">Sell</a>
            </td>
        </tr>
    </c:forEach>

    </tbody>

</table>
        </div>
    </div>
</div>

</body>
</html>
