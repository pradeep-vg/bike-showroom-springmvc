package com.ssbsoft.showroomweb.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.util.Date;

/**
 * Baseentity model is POJO class
 * it contains common fields  variables
 * */

@MappedSuperclass
public class BaseEntity {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;
	@ManyToOne(cascade = CascadeType.MERGE , fetch = FetchType.LAZY)
	@JoinColumn(name = "created_by" , updatable = false)
	private User createdBy;

	@ManyToOne(cascade= CascadeType.MERGE , fetch = FetchType.LAZY)
	@JoinColumn(name = "updated_by")
	private User updatedBy;
	@Column(name = "created_on", updatable = false)
	private Date createdOn;
	@Column(name = "updated_on")
	private Date updatedOn;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}


	@Override
	public String toString() {
		return "BaseEntity{" +
			"id=" + id +
			", createdBy=" + createdBy +
			", updatedBy=" + updatedBy +
			", createdOn=" + createdOn +
			", updatedOn=" + updatedOn +
			'}';
	}
}
