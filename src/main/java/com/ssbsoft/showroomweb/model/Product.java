package com.ssbsoft.showroomweb.model;




import com.ssbsoft.showroomweb.util.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Product model is POJO class
 * it contains getters and setters of Order variables
 */
@Entity
@Table(name="product")
public class Product extends BaseEntity{

    @Column(name = "product_name",unique = true)
    private String productName;
    @Column(name = "unit_price")
    private int unitPrice;
    @Column(name = "last_update")
    private Date lastUpdate;

  /*  @OneToMany(mappedBy = "stockId.product",cascade = CascadeType.PERSIST,fetch = FetchType.EAGER)
    private Set<Stock> stock;*/

    @Column(name = "cc")
    private int cc;
    @Column(name="mileage")
    private float mileage;
    @Column(name = "weight")
    private float weight;
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public int getCc() {
        return cc;
    }

    public void setCc(int cc) {
        this.cc = cc;
    }

    public float getMileage() {
        return mileage;
    }

    public void setMileage(float mileage) {
        this.mileage = mileage;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productName='" + productName + '\'' +
                ", unitPrice=" + unitPrice +
                ", lastUpdate=" + lastUpdate +
                ", cc=" + cc +
                ", mileage=" + mileage +
                ", weight=" + weight +
                ", status=" + status +
                '}';
    }
}
