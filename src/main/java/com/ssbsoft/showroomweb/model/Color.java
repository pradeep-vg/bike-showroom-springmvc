package com.ssbsoft.showroomweb.model;

import javax.persistence.*;
import java.util.Set;


@Entity
@Table(name = "color")
public class Color {
	@Id
	@GeneratedValue
	private int id;


	@Column(name = "color")
	private String color;

	/*@OneToMany(mappedBy = "stockId.color",cascade = CascadeType.ALL)
	private Set<Stock> stock;*/

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	/*public Set<Stock> getStock() {
		return stock;
	}

	public void setStock(Set<Stock> stock) {
		this.stock = stock;
	}*/

	@Override
	public String toString() {
		return "Color{" +
			"id=" + id +
			", color='" + color + '\'' +
		/*	", stock=" + stock +*/
			'}';
	}
}
