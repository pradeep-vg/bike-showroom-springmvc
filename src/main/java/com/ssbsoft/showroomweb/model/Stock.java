package com.ssbsoft.showroomweb.model;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "stock",uniqueConstraints = {@UniqueConstraint(columnNames = {"color_id","product_id" })})
public class Stock extends BaseEntity{


	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "color_id")
	private Color color;

	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "product_id")
	private Product product;

	@Column
	private int quantity;

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}


	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}



	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "Stock{" +

			", colour=" + color +
			", quantity=" + quantity +
			", product=" + product +
			'}';
	}
}