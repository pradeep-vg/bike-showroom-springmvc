package com.ssbsoft.showroomweb.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;


/**
 * <p>
 * salesDetail model is POJO class
 * it contains getters and setters of salesDetail variables
 * </p>
 */
@Entity
@Table(name = "sales")
public class Sales {

    @Id
    @GeneratedValue
    private int id;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "color")
    private Color color;
    @Column(name = "quantity")
    private int quantity;
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id")
    private Product product;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id")
    private User customer;
    @Column(name = "price")
    private int unitPrice;
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "billedBy")
    private User billedBy;
    @Column(name = "billedOn")
    private Date billedOn;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }

    public User getBilledBy() {
        return billedBy;
    }

    public void setBilledBy(User billedBy) {
        this.billedBy = billedBy;
    }

    public Date getBilledOn() {
        return billedOn;
    }

    public void setBilledOn(Date billedOn) {
        this.billedOn = billedOn;
    }

    @Override
    public String toString() {
        return "SalesDetail{" +
            "id=" + id +
            ", color=" + color +
            ", quantity=" + quantity +
            ", product=" + product +
            ", customer=" + customer +
            ", unitPrice=" + unitPrice +
            ", billedBy=" + billedBy +
            ", billedOn=" + billedOn +
            '}';
    }
}




