package com.ssbsoft.showroomweb.controller;

import com.ssbsoft.showroomweb.model.Color;
import com.ssbsoft.showroomweb.model.Stock;

import com.ssbsoft.showroomweb.service.ProductService;
import com.ssbsoft.showroomweb.service.StockService;
import com.ssbsoft.showroomweb.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * stock controller receives and send stock details from JSP pages and send to Service Vice versa
 *
 * @author Pradeep
 */
@Controller
@RequestMapping("/stock")
public class StockController {
	@Autowired
	private StockService stockService;
	@Autowired
	private ProductService productService;

	/**
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/{id}",method = RequestMethod.GET)
	public String addStock(@PathVariable("id") int id,ModelMap modelMap) {
		modelMap.addAttribute("product", productService.getProductById(id));
		modelMap.addAttribute(Constants.STOCK,new Stock());
		modelMap.addAttribute(Constants.COLOR,stockService.getColors());
		return "addStock";
	}

	/**
	 * @param id
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/productDetails/{id}" , method = RequestMethod.GET)
	public String viewProductDetails(@PathVariable int id , ModelMap modelMap) {
		modelMap.addAttribute("stock",stockService.getStockByProductId(id));
		System.out.println("========product details ======="+stockService.getStockByProductId(id));
		return "viewProductDetails";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String addStock(@ModelAttribute("stock") Stock stock,ModelMap modelMap){
		stockService.addStock(stock);
		modelMap.addAttribute(Constants.ERROR,"Stock Updated Successfully");
		return "redirect:/product";
	}


}
