package com.ssbsoft.showroomweb.controller;

import com.ssbsoft.showroomweb.model.Product;
import com.ssbsoft.showroomweb.service.ProductService;
import com.ssbsoft.showroomweb.service.StockService;
import com.ssbsoft.showroomweb.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * Product controller receives and send product details from JSP pages and send to Service Vice versa
 *
 * @author Pradeep
 */
@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;
    @Autowired
    private StockService stockService;
    /**
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String viewProducts(ModelMap modelMap) {
        modelMap.addAttribute("products", productService.getProducts());
        return "viewProducts";

       }

    /**
     *
     * @param id
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/productDetails/{id}" , method = RequestMethod.GET)
    public String viewProductDetails(@PathVariable int id , ModelMap modelMap) {
        modelMap.addAttribute("product", productService.getProductById(id));
        modelMap.addAttribute("stock",stockService.getStockByProductId(id));
        return "viewProductDetails";
    }

    /**
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/productForm", method = RequestMethod.GET)
    public String createProductForm(ModelMap modelMap) {
        modelMap.addAttribute(Constants.PRODUCT, new Product());
        return "addProduct";
    }

    /**
     * @param product
     * @param modelMap
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public String addProduct(@ModelAttribute("product") Product product, ModelMap modelMap) {
        if (product.getId() == 0) {
            productService.addProduct(product);
        } else {
            productService.updateProduct(product);
        }
        return "redirect:/product";
    }

    /**
     * @param id
     * @return
     */
    @RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable int id) {
        productService.deleteProduct(id);
        return "redirect:/product";
    }

    /*
     * @param id
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
    public String getProductById(@PathVariable int id, ModelMap modelMap) {
        modelMap.addAttribute("product", productService.getProductById(id));
        return "addProduct";
    }

}
