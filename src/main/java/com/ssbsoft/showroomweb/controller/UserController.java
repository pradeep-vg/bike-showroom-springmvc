package com.ssbsoft.showroomweb.controller;

import com.ssbsoft.showroomweb.model.User;
import com.ssbsoft.showroomweb.service.impl.UserServiceImpl;
import com.ssbsoft.showroomweb.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * User controller receives and send user details from JSP pages and send to Service Vice versa
 *
 * @author Pradeep
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserServiceImpl userService;

    /**
     * createUserForm
     *
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/createPage", method = RequestMethod.GET)
    public String createUserForm(ModelMap modelMap) {
        modelMap.addAttribute(Constants.USER, new User());
        return "createOrUpdateUser";
    }

    /**
     * @param user
     * @param modelMap
     * @return
     */
    @RequestMapping( value = "/createOrUpdate" , method = RequestMethod.POST)
    public String addUser(@ModelAttribute("user") User user, ModelMap modelMap) {
        try {
            if (user.getId() == 0) {
                String role = userService.createUser(user);


                return "redirect:/user/"+role;
            } else {

                userService.updateUser(user);
            }
            if (userService.getByLoggedInUser().getId() == user.getId()) {
                modelMap.addAttribute(Constants.ERROR, "Profile Updated Successfully");
                return "createOrUpdateUser";
            } else {
                modelMap.addAttribute(Constants.ERROR, "User Details Updated");
                return "redirect:/user/customer" /*+ user.getRole().getRole().toLowerCase()*/;
            }
        }catch (Throwable e){
            modelMap.addAttribute(Constants.ERROR, "Username Already Exists");
            return "createOrUpdateUser";
        }
    }

    /**

     * @param modelMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/customer", method = RequestMethod.GET)
    public String viewCustomer(ModelMap modelMap) throws Exception {
        modelMap.addAttribute("customer", userService.getUsers(3));
        System.out.println(userService.getUsers(1));
        return "viewCustomers";

    }
    /**

     * @param modelMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/staff", method = RequestMethod.GET)
    public String viewStaff(ModelMap modelMap) throws Exception {
        modelMap.addAttribute("users", userService.getUsers(2));

        return "viewStaffs";

    }


    /**
     * @param id
     * @return
     */
    @RequestMapping(value = "/deleteUser/{id}", method = RequestMethod.GET)
    public String deleteUser(@PathVariable int id) {
        User user = userService.getUserById(id);
        userService.deleteUser(user);
        return "redirect:/user/customer" /*+ user.getRole().getRole().toLowerCase()*/;
    }

    /**
     * @param id
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/updateUser/{id}", method = RequestMethod.GET)
    public String setUserDetails(@PathVariable int id, ModelMap modelMap) {
        modelMap.addAttribute("user", userService.getUserById(id));
        return "createOrUpdateUser";
    }
}