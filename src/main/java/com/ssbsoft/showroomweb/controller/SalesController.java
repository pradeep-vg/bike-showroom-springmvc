package com.ssbsoft.showroomweb.controller;

import com.ssbsoft.showroomweb.model.Sales;
import com.ssbsoft.showroomweb.service.ProductService;
import com.ssbsoft.showroomweb.service.SalesService;
import com.ssbsoft.showroomweb.service.StockService;
import com.ssbsoft.showroomweb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Product controller receives and send product details from JSP pages and send to Service Vice versa
 *
 * @author Pradeep
 */
@Controller
@RequestMapping(value = "/sales")
public class SalesController {

	@Autowired
	private SalesService salesService;
	@Autowired
	private UserService userService;
	@Autowired
	private StockService stockService;
	@Autowired
	private ProductService productService;

	/**
	 * @param id
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public String salesDetail(@PathVariable("id") int id, ModelMap modelMap) {

		modelMap.addAttribute("salesDetail",new Sales());
		modelMap.addAttribute("product", productService.getProductById(id));
		modelMap.addAttribute("stock",stockService.getStockByProductId(id));
		modelMap.addAttribute("customer", userService.getUsers(3));
		System.out.println("===============stock==============="+stockService.getStockByProductId(id));
		System.out.println("===============user==============="+userService.getUsers(3));
		return "salesEntry";

	}
	@RequestMapping(method = RequestMethod.GET)
	public String viewSalesDetails(ModelMap modelMap){
		modelMap.addAttribute("salesDetails", salesService.getSalesDetails());
		return "viewSalesDetails";
	}

	/**
	 * @param sales
	 * @return

	 */
	@RequestMapping(method = RequestMethod.POST)
	public String addOrder(@ModelAttribute("salesDetails") Sales sales) {
		System.out.println("=========salesDetail=========="+ sales);
		salesService.setSalesDetails(sales);
		return "redirect:/sales";
	}


}
