package com.ssbsoft.showroomweb.util;

public class Constants {
    public static final int ZERO = 0;
    public static final int ONE = 1;
    public static final int TWO = 2;
    public static final int THREE = 3;
    public static final int FOUR = 4;
    public static final int FIVE = 5;
    public static final int SIX = 6;
    public static final int SEVEN = 7;
    public static final int EIGHT = 8;
    public static final int NINE = 9;
    public static final int TEN = 10;
    public static final String

    //Common Constant Strings
            LOGIN = "login",
            LOGOUT = "logout",
            ERROR = "error",
            STATUS = "status",
            CREATED_BY = "createdBy",
            UNAUTHORIZED_ACCESS = "Login Failed! Login Again",
            INVAlID_PASSWORD = "Invalid Username or Password! Try Again !!",
            THANK_YOU = "Thank You !! Welcome Again",
            ACTION_TO_PERFORM = "actionToPerform",
            TIME_OUT="YOUR SESSION TIMED OUT! Kindly Login Again",

    //User Constant Strings
            FIRST_NAME = "firstName",
            ROLE = "role",
            ROLES = "roles",
            ID = "id",
            USER_ID = "userId",
            LAST_NAME = "lastName",
            MOBILE_NUMBER = "mobile",
            EMAIL = "email",
            ADDRESS = "address",
            USERNAME = "username",
            PASSWORD = "password",

            CURRENT_USER = "currentUser",
            STAFFS="staffs",
            VIEW_USERS = "userServlet?actionToPerform=viewUsers",
            VIEW_STAFFS = "userServlet?actionToPerform=viewStaffs",
            CREATE_USER = "createUser",
            UPDATE_USER = "updateUser",
            USERNAME_EXISTS = "Username already Exists ! Try Creating With New Username",
            USER = "user", USERS = "users",
            VIEW_USER="viewUsers",
            VIEW_STAFF="viewStaffs",
            DELETE_USER="deleteUser",

    //Product Constant Strings
            ADD_PRODUCT = "addProduct",
            PRODUCT_NAME = "productName",
            UNIT_PRICE = "unitPrice",
            UPDATE_PRODUCT = "updateProduct",
            DELETE_PRODUCT = "deleteProduct",
            VIEW_PRODUCT = "viewProducts",
            PRODUCTS = "products",
            PRODUCT = "product",
            COLOR = "color",
            VIEW_PRODUCTS = "/productServlet?actionToPerform=viewProducts",
            PRODUCT_NAME_EXISTS = "ProductName already Exists ! Enter New Product or Just Update the Stock!",

    //Sales Constant Strings
             PRICE = "price",
            QUANTITY = "quantity",
            TOTAL_PRICE = "totalPrice",
            ORDER_DETAILS = "getOrderDetailsInCart",
            ADD_TO_CART = "addToCart",
            ORDERS = "orders",
            VIEW_ORDERS = "/orderServlet?actionToPerform=viewOrders",
            UPDATED_BY = "updatedBy",
            STOCK_ID = "stock_id",
            STOCK = "stock",
            ADD_ORDER = "addOrder",
            CLEAR_CART = "clearCart",
            REMOVE_ITEM = "removeItem";



}