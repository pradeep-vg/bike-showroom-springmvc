package com.ssbsoft.showroomweb.util;

import com.ssbsoft.showroomweb.model.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.Properties;

/**
 * HibernateUtil is used to connect with database
 * it opens the session
 * @author Pradeep
 */
public class HibernateUtil {

    private static SessionFactory sessionFactory = createSessionFactory();

    /**
     * creating the session factory according to given properties
     * @return SessionFactory
     * @throws HibernateException
     */
    private static SessionFactory createSessionFactory() throws HibernateException {
        try {
            Properties properties = new Properties();
            properties.setProperty("hibernate.connection.url", "jdbc:mysql://localhost:3306/showroomweb_spring?createDatabaseIfNotExist=TRUE");
            properties.setProperty("hibernate.connection.username", "root");
            properties.setProperty("hibernate.connection.password", "root");
            properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
            properties.setProperty("hibernate.connection.driver_class", "com.mysql.cj.jdbc.Driver");
            properties.setProperty("hibernate.hbm2ddl.auto", "update");
            properties.setProperty("hibernate.show_sql", "true");
           /* properties.setProperty("hibernate.format_sql","true");*/

            Configuration configure = new Configuration().addProperties(properties)
                    .addAnnotatedClass(Role.class)
                    .addAnnotatedClass(User.class)
                    .addAnnotatedClass(Product.class)
                    .addAnnotatedClass(Stock.class)
                    .addAnnotatedClass(Sales.class)
                    .addAnnotatedClass(Color.class);
            sessionFactory =  configure.buildSessionFactory();
            System.out.println("sessionFactory: " + sessionFactory);
            return sessionFactory;

        } catch (Throwable ex) {
            ex.printStackTrace();

            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * createSession is returning a session
     * @return Session
     */
    public static Session createSession() {
        return sessionFactory.openSession();
    }
}
