package com.ssbsoft.showroomweb.util;


import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Date util is used to Getting current date
 * @author  Pradeep
 */
@Component
public class DateUtil {

    /**
     * Getting Current
     * @return sqlDate
     */
    public static Date getCurrentDate(){
        Date currentDate=new Date();
        Date Date = new Date(currentDate.getTime());
        return Date;
    }
}
