package com.ssbsoft.showroomweb.service;

import com.ssbsoft.showroomweb.model.Sales;

import java.util.List;

public interface SalesService {
	Sales setProductById(int salesId);
	List<Sales> getSalesDetails();
	void setSalesDetails (Sales sales);
}
