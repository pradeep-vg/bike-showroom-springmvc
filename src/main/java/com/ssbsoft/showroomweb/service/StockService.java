package com.ssbsoft.showroomweb.service;


import com.ssbsoft.showroomweb.model.Color;
import com.ssbsoft.showroomweb.model.Product;
import com.ssbsoft.showroomweb.model.Stock;

import java.util.List;

public interface StockService {
	void addStock(Stock stock);
	List<Stock> getStockByProductId(int id);
	Stock getStock(int productId, int colorId);
	List<Color> getColors();
}
