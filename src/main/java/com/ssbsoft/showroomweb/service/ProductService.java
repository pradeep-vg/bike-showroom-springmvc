package com.ssbsoft.showroomweb.service;

import com.ssbsoft.showroomweb.model.Product;

import java.util.List;

public interface ProductService {
	void deleteProduct(int id);
	Product getProductById(int productId);
	List<Product> getProducts();
	void updateProduct(Product product);
	void addProduct(Product product);

}
