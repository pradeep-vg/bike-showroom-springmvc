package com.ssbsoft.showroomweb.service.impl;

import com.ssbsoft.showroomweb.dao.impl.ProductDaoImpl;
import com.ssbsoft.showroomweb.model.Product;
import com.ssbsoft.showroomweb.service.ProductService;
import com.ssbsoft.showroomweb.util.DateUtil;
import com.ssbsoft.showroomweb.util.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.model.AlreadyExistsException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Product service is used to get object from product Servlet send it to dao vice versa
 *
 * @author Pradeep
 */
@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    ProductDaoImpl productDao;
    @Autowired
    DateUtil dateUtil;
    @Autowired
    UserServiceImpl userService;
    /**
     * passing product object to dao to perform delete operation
     *
     * @param id
     */
    public void deleteProduct(int id) {
        Product product=getProductById(id);
        product.setUpdatedBy(userService.getByLoggedInUser());
        product.setLastUpdate(dateUtil.getCurrentDate());
        product.setStatus(Status.INACTIVE);
        productDao.saveOrUpdate(product);
    }

    /**
     * getting product from dao by passing id
     *
     * @param productId
     * @return product
     */
    public Product getProductById(int productId) {
        return productDao.getProductById(productId);
    }

    /**
     * getting list of products
     *
     * @return
     */
    public List<Product> getProducts() {
        return productDao.getProducts();
    }

    /**
     * passing product object to dao to perform update operation
     *
     * @param product
     */
    public void updateProduct(Product product) {
        product.setUpdatedBy(userService.getByLoggedInUser());
        product.setLastUpdate(dateUtil.getCurrentDate());
        productDao.saveOrUpdate(product);
    }
    /**
     * passing product object to dao to perform update operation after Order
     *
     * @param product
     */
    public void updateProductAfterOrder(Product product) {
        productDao.saveOrUpdate(product);
    }



    /**
     * passing product object to productDao and stockDao to perform add operation
     *
     * @param product
     */
    public void addProduct(Product product) {
        if(checkProductNameValidation(product)){
        product.setCreatedBy(userService.getByLoggedInUser());
        product.setCreatedOn(dateUtil.getCurrentDate());
        productDao.saveOrUpdate(product);}
        else throw new AlreadyExistsException("ProductName Already Exists");
    }



   private boolean checkProductNameValidation(Product product) {
       int count = 0;
       Product product1 = productDao.getProductById(product.getId());
       System.out.println("product1 ----- :"+product1);
       if (product1 == null )
           count++;
       assert false;
       return count == 1;

   }

}
