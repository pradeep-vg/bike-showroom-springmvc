package com.ssbsoft.showroomweb.service.impl;


import com.ssbsoft.showroomweb.dao.StockDao;
import com.ssbsoft.showroomweb.model.Color;
import com.ssbsoft.showroomweb.model.Product;
import com.ssbsoft.showroomweb.model.Stock;
import com.ssbsoft.showroomweb.service.StockService;
import com.ssbsoft.showroomweb.service.UserService;
import com.ssbsoft.showroomweb.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StockServiceImpl implements StockService {
	@Autowired
	UserService userService;

	@Autowired
	StockDao stockDao;




	/**
	 * add stock is to add or update stock
	 * @param stock
	 */

	public void addStock(Stock stock){
		stock.setCreatedBy(userService.getByLoggedInUser());
		stock.setCreatedOn(DateUtil.getCurrentDate());
		stockDao.saveOrUpdate(stock);
	}

	public List<Color> getColors(){
		return 	stockDao.getColors();
	}
	/**
	 * getStockById returns the list of stock with colors
	 * @param id
	 * @return
	 */
	public List<Stock> getStockByProductId(int id){
		return stockDao.getStockByProductId(id);
	}

	/**
	 * getStock is to get the individual stock of product based on color
	 * @param productId
	 * @param colorId
	 * @return
	 */
	public Stock getStock(int productId, int colorId){

		return stockDao.getStock(productId,colorId);
	}
}
