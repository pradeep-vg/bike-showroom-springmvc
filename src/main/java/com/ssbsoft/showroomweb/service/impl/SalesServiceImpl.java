package com.ssbsoft.showroomweb.service.impl;


import com.ssbsoft.showroomweb.dao.impl.SalesDaoImpl;
import com.ssbsoft.showroomweb.model.Sales;
import com.ssbsoft.showroomweb.model.Stock;
import com.ssbsoft.showroomweb.model.User;
import com.ssbsoft.showroomweb.service.EmailService;
import com.ssbsoft.showroomweb.service.SalesService;
import com.ssbsoft.showroomweb.service.StockService;
import com.ssbsoft.showroomweb.service.UserService;
import com.ssbsoft.showroomweb.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * salesDetail service is used to get object from sales Servlet send it to dao vice versa
 *
 * @Pradeep
 */
@Service
public class SalesServiceImpl implements SalesService {
    @Autowired
    private ProductServiceImpl productService;
    @Autowired
    private SalesDaoImpl salesDao;
    @Autowired
    private UserService userService;
    @Autowired
    private StockService stockService;
    @Autowired
    private EmailService emailService;

    public List<Sales> getSalesDetails(){
        return salesDao.getSalesDetails();
    }
    /**
     *
     * @param salesId
     * @return
     */
    public Sales setProductById(int salesId) {
        System.out.println("=======set product by id======");
        Sales sales = new Sales();
        sales.setProduct(productService.getProductById(salesId));
        return sales;
    }

    /**
     * setSalesDetails is used to save the salesEntry and reducing the existing stock of the product
     * @param sales
     */
    public void setSalesDetails (Sales sales){
        sales.setBilledBy(userService.getByLoggedInUser());
        sales.setQuantity(1);
        sales.setBilledOn(DateUtil.getCurrentDate());
        Stock stock = stockService.getStock(sales.getProduct().getId(), sales.getColor().getId());
        System.out.println("=====stock===="+stock);
        User customer = userService.getUserById(sales.getCustomer().getId());
        System.out.println("====customer===="+userService.getUserById(sales.getCustomer().getId()));
        System.out.println("======mail id====="+customer.getEmail());
        emailService.sendMail(customer.getEmail(),
                            "Greetings from yamaha showroom",
                                "Thank you for purchasing from yamaha showroom,"+
                                        "\nFor more details log in to your account" +
                                        "\nUsername:"+customer.getUsername()+
                                        "\nPassword:"+customer.getPassword());
        salesDao.setSalesDetails(sales);
        stock.setQuantity(stock.getQuantity()-1);
        stockService.addStock(stock);
    }

}
