package com.ssbsoft.showroomweb.service.impl;

import com.ssbsoft.showroomweb.dao.UserDao;

import com.ssbsoft.showroomweb.model.User;
import com.ssbsoft.showroomweb.service.UserService;
import com.ssbsoft.showroomweb.util.Constants;
import com.ssbsoft.showroomweb.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.model.AlreadyExistsException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * User service is used to get object from User Servlet send it to UserDao vice versa
 *
 * @author Pradeep
 */
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userDao;
/*
	@Autowired
	private DateUtil dateUtil;
*/

	/**
	 * passing user object to dao to perform add operation
	 *
	 * @param user
	 * @return
	 */
	public String createUser(User user) {
		if (checkUsernameValidation(user)) {
            user.setCreatedBy(getByLoggedInUser());
            user.setCreatedOn(DateUtil.getCurrentDate());
			userDao.saveOrUpdate(user);
			/*if(user.getRole().getId() == 2)
			return "staff";
			else*/
				return "customer";
		} else {
			throw new AlreadyExistsException("Username Already Exists");
		}
	}

	/**
	 * getting list of (users) from userDao
	 *
	 * @param roleId
	 * @return list of users and staffs
	 */
	public List<User> getUsers(int roleId) {
		return userDao.getUsers(roleId);
	}

	/**
	 * passing user object to dao to perform add operation
	 *
	 * @param user
	 */
	public void updateUser(User user) {
		user.setUpdatedBy(getByLoggedInUser());
		user.setUpdatedOn(DateUtil.getCurrentDate());
		userDao.saveOrUpdate(user);
	}

	/**
	 * passing user object to dao to perform delete operation
	 *
	 * @param user
	 */
	public void deleteUser(User user) {
		/*user.setUpdatedBy(getByLoggedInUser().getId());
		user.setUpdatedOn(dateUtil.getCurrentDate());
		user.setStatus(Constants.ONE);
		userDao.saveOrUpdateFunction(user);*/
		userDao.delete(user);
	}

	/**
	 * getting user from dao by passing user id
	 *
	 * @param id return user
	 */
	public User getUserById(int id) {
		return userDao.getUserById(id);
	}

	/**
	 * getting user from dao by passing username
	 *
	 * @param user
	 * @return user
	 */
	public User getUserByUsername(User user) {
		return userDao.getUserByUsername(user.getUsername());
	}

	/**
	 * getting current logged in user
	 *
	 * @return user
	 */
	public User getByLoggedInUser() {
		return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	/**
	 * validating username for Availability
	 *
	 * @param user
	 * @return boolean value
	 */

	private boolean checkUsernameValidation(User user) {

		User existingUser = userDao.getUserByUsername(user.getUsername());
		System.out.println("user1 ----- :" + existingUser);

		return (null == existingUser);
	}
}