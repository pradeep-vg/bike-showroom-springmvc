package com.ssbsoft.showroomweb.service;

import com.ssbsoft.showroomweb.model.User;

import java.util.List;

public interface UserService {
	String createUser(User user);
	List<User> getUsers(int roleId);
	void updateUser(User user);
	void deleteUser(User user);
	User getUserById(int id);
	User getUserByUsername(User user);
	User getByLoggedInUser();
}
