package com.ssbsoft.showroomweb.dao;

import com.ssbsoft.showroomweb.model.Sales;

import java.util.List;

public interface SalesDao {
	void setSalesDetails(Sales salesDetails);
	List<Sales> getSalesDetails();
}
