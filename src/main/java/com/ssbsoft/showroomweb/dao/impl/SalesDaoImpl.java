package com.ssbsoft.showroomweb.dao.impl;

import com.ssbsoft.showroomweb.dao.SalesDao;
import com.ssbsoft.showroomweb.model.Sales;
import com.ssbsoft.showroomweb.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * salesDetail Dao is used to get object from salesDetail Service send it to Database vice versa
 *
 * @author Pradeep
 */
@Repository
public class SalesDaoImpl implements SalesDao {
    /**
     * set getsalesDetails method is performing inserting sales details to sales_detail table
     *
     * @param salesDetails
     */
    public void setSalesDetails(Sales salesDetails) {
        Session session = HibernateUtil.createSession();
        Transaction transaction = session.beginTransaction();
       /* for (SalesDetail salesDetail : salesDetails) {
            session.save(salesDetail);
        }*/
       session.save(salesDetails);
        transaction.commit();
        session.close();
    }

    /**
     * getsalesDetails method is used to get list of get details from sales_detail table corresponding to salesId
     *
     * @return list of sales
     **/
    public List<Sales> getSalesDetails() {
        Session session = HibernateUtil.createSession();

        Criteria criteria = session.createCriteria(Sales.class);
        List<Sales> sales = criteria.list();
         session.close();
        System.out.println("====salesDetailsfrom dao===="+sales);
        return sales;
    }

}
