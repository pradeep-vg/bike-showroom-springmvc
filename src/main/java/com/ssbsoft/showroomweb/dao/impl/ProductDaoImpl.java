package com.ssbsoft.showroomweb.dao.impl;

import com.ssbsoft.showroomweb.dao.ProductDao;
import com.ssbsoft.showroomweb.model.Product;
import com.ssbsoft.showroomweb.util.Constants;
import com.ssbsoft.showroomweb.util.HibernateUtil;
import com.ssbsoft.showroomweb.util.Status;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * product Dao is used to get object from product Service send it to Database vice versa
 *
 * @author Pradeep
 */
@Repository
public class ProductDaoImpl implements ProductDao {
    @Autowired
    private CommonDaoImpl commonDao;

    /**
     * saveOrUpdateFunction is inserting,deleting,updating product elements into products table
     *
     * @param product
     * @return product
     */
    public void saveOrUpdate(Product product) {
        commonDao.saveOrUpdate(product);
    }

    /**
     * getting product details from product table by passing id
     *
     * @param id
     * @return product
     */
    public Product getProductById(int id) {
        Session session = HibernateUtil.createSession();
        Product product = (Product) session.get(Product.class, id);
        session.close();
        return product;
    }

    /**
     * getting all info from product table
     *
     * @return list of products
     */
    public List<Product> getProducts() {
        Session session = HibernateUtil.createSession();
        Criteria criteria = session.createCriteria(Product.class);
        criteria.add(Restrictions.eq(Constants.STATUS, Status.ACTIVE));

        List<Product> products = criteria.list();
        session.close();
        return products;
    }
}
