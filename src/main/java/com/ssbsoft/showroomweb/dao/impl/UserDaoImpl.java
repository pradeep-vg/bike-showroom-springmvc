package com.ssbsoft.showroomweb.dao.impl;

import com.ssbsoft.showroomweb.dao.UserDao;
import com.ssbsoft.showroomweb.model.User;
import com.ssbsoft.showroomweb.util.*;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.ssbsoft.showroomweb.util.HibernateUtil.createSession;


/**
 * user Dao is used to get object from product Service send it to Database vice versa
 *
 * @Pradeep
 */
@Repository
public class UserDaoImpl implements UserDao {
    @Autowired
    private CommonDaoImpl commonDao;

    /**
     * <p>
     * saveOrUpdateFunction is used to insert,update,delete user details in user table
     * </p>
     * @param user - Used to save user details.
     */
    public void saveOrUpdate(User user) {
        commonDao.saveOrUpdate(user);
    }

    public void delete(User user) {
        commonDao.delete(user);
    }
    /**
     * get user by id is used to perform return user object corresponding to param id
     *
     * @param id
     * @return user
     */
    public User getUserById(int id) {
        Session session = createSession();
        User user = (User) session.get(User.class, id);
        session.close();
        return user;
    }

    /**
     * getUsers is used return user objects where role id =3
     *
     * @return list of users
     */
    public List<User> getUsers(int roleId) {
        Session session = createSession();
        Criteria criteria = session.createCriteria(User.class);
        criteria.add(Restrictions.eq(Constants.STATUS, Status.ACTIVE));
        criteria.createAlias(Constants.ROLES, Constants.ROLES).add(Restrictions.eq("roles.id", roleId));
        List<User> users = criteria.list();
        session.close();
        return users;
    }

    /**
     * getting user details from user table by using username and password
     *
     * @param username
     * @return user
     */
    public User getUserByUsername(String username) {
        Session session = HibernateUtil.createSession();
        Criteria criteria = session.createCriteria(User.class)
                /*.createAlias("roles", "role_name")*/
                .add(Restrictions.eq(Constants.USERNAME, username));
        User user = (User) criteria.uniqueResult();
        session.close();
        return user;
    }
}
