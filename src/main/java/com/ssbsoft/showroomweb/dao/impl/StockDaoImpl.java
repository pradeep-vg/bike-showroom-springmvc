package com.ssbsoft.showroomweb.dao.impl;

import com.ssbsoft.showroomweb.dao.CommonDao;
import com.ssbsoft.showroomweb.dao.StockDao;
import com.ssbsoft.showroomweb.model.Color;
import com.ssbsoft.showroomweb.model.Product;
import com.ssbsoft.showroomweb.model.Stock;
import com.ssbsoft.showroomweb.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.HibernateError;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * stock Dao is used to get object from stock Service send it to Database vice versa
 *
 * @author Pradeep
 */
@Repository
public class StockDaoImpl implements StockDao {
	@Autowired
	CommonDao commonDao;

	/**
	 * saveOrUpdate method is used to save or update the stock setails in database
	 *
	 * @param stock
	 */
	@Override
	public void saveOrUpdate(Stock stock) {
		commonDao.saveOrUpdate(stock);
	}

	public List<Stock> getStockByProductId(int id) {
		List<Stock> stock = null;
		try {
			Session session = HibernateUtil.createSession();
			Criteria criteria = session.createCriteria(Stock.class);
			criteria.add(Restrictions.eq("product.id", id));

			stock = criteria.list();
			session.close();

			return stock;
		} catch (HibernateError hb) {

			throw new HibernateException("hibernate Exception");

		}

	}

	public Stock getStock(int productId, int colorId) {
		Stock stock = null;
		try{
			Session session = HibernateUtil.createSession();
			Criteria criteria =session.createCriteria(Stock.class);
			criteria.add(Restrictions.eq("product.id",productId))
			.add(Restrictions.eq("color.id",colorId));

			stock = (Stock) criteria.uniqueResult();
			session.close();
			return stock;
		}catch (Exception e){
			e.printStackTrace();
		}
		return stock;
	}
	public List<Color> getColors(){
		Session session = HibernateUtil.createSession();
		Criteria criteria = session.createCriteria(Color.class);
		List<Color> colors = criteria.list();
		return colors;
	}
}
