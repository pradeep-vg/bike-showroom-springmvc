package com.ssbsoft.showroomweb.dao;

import com.ssbsoft.showroomweb.model.User;

import java.util.List;

public interface UserDao {
	void saveOrUpdate(User user);
	User getUserById(int id);
	List<User> getUsers(int roleId);
	User getUserByUsername(String username);
	void delete(User user);
}
