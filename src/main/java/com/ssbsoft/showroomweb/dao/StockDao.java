package com.ssbsoft.showroomweb.dao;

import com.ssbsoft.showroomweb.model.Color;
import com.ssbsoft.showroomweb.model.Product;
import com.ssbsoft.showroomweb.model.Stock;

import java.util.List;

public interface StockDao {
	void saveOrUpdate(Stock stock);
	List<Stock> getStockByProductId(int id);
	Stock getStock(int productId, int colorId);
	List<Color> getColors();
}
