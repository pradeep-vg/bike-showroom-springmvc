package com.ssbsoft.showroomweb.dao;

public interface CommonDao {
	void saveOrUpdate(Object object);
	void delete(Object object);
}
