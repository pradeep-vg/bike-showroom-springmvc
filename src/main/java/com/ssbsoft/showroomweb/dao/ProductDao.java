package com.ssbsoft.showroomweb.dao;

import com.ssbsoft.showroomweb.model.Product;

import java.util.List;

public interface ProductDao {
	void saveOrUpdate(Product product);
	Product getProductById(int id);
	List<Product> getProducts();
}
