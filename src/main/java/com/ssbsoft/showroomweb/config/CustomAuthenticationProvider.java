package com.ssbsoft.showroomweb.config;


import com.ssbsoft.showroomweb.model.User;
import com.ssbsoft.showroomweb.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;


@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UserServiceImpl userService;
    @RequestMapping (value = "/session")
    @Override

    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {
        System.out.println("----Authentication Provider------");
        String name = authentication.getPrincipal().toString();
        System.out.println("Name = " + name);
        String password = authentication.getCredentials().toString();
        System.out.println("password = " + password);
        User user = new User();
        user.setUsername(name);
        User userDetails;
        try {
            userDetails = this.userService.getUserByUsername(user);
            if (StringUtils.isEmpty(password)) {
                System.out.println("------password empty-------");
                throw new BadCredentialsException("No Username and/or Password Provided.");
            } else if (!userDetails.getPassword().equals(password)) {
                System.out.println("------invalid login-------");
                throw new BadCredentialsException("Invalid Username or Password,Please check the entry.");
            } else {
                System.out.println("--------LOGIN SUCCESS---------");
                System.out.println("======logged in user====="+userDetails);
                System.out.println("=====roles====="+userDetails.getRoles());
                return new UsernamePasswordAuthenticationToken(
                        userDetails, password, new ArrayList<>());

            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new BadCredentialsException(e.getMessage());
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
