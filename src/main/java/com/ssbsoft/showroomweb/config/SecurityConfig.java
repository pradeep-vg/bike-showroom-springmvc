package com.ssbsoft.showroomweb.config;

import com.ssbsoft.showroomweb.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.web.context.request.RequestContextListener;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@Configuration
@EnableWebSecurity
@ComponentScan("com.ssbsoft.showroomweb")
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private CustomAuthenticationProvider customAuthProvider;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        System.out.println("........9...AuthenticationManagerBuilder....");
        auth.authenticationProvider(customAuthProvider);
    }

    @Bean
    @Order(0)
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        System.out.println("........10...HttpSecurity....");
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/assets/**").permitAll()
                .antMatchers("/app/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/")
                .loginProcessingUrl("/session")
                .usernameParameter("username")
                .passwordParameter("password")
                .defaultSuccessUrl("/home")
                .failureUrl("/login?error=true")
                .successHandler(getAuthenticationSuccessHandler())
                .failureHandler(getAuthenticationFailureHandler())
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .logoutSuccessHandler(getLogoutSuccessHandler())
                .and().headers().frameOptions().sameOrigin()
                .and()
                .csrf().disable();

    }

    private AuthenticationSuccessHandler getAuthenticationSuccessHandler() {

        //since we have created our custom success handler, its up to us to where
        //we will redirect the user after successfully login


        return (request, response, authentication) -> {
            HttpSession session = request.getSession();
            User authUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            session.setAttribute("currentUser", authUser);
            //set our response to OK status
            response.setStatus(HttpServletResponse.SC_OK);
            request.getSession().setMaxInactiveInterval(60 * 60 * 24);
            response.setStatus(HttpStatus.OK.value());
            response.sendRedirect("home");
        };
    }

    private AuthenticationFailureHandler getAuthenticationFailureHandler() {
        return (request, response, exception) -> {
            System.out.println("---failure handler----");
            System.out.println("=---------Context Path-------=  "+request.getContextPath());
            response.sendRedirect("login");
        };
    }

    private LogoutSuccessHandler getLogoutSuccessHandler() {
        return (request, response, authentication) -> {
            response.setStatus(HttpStatus.NO_CONTENT.value());
            response.sendRedirect("/login");
        };
    }

}

