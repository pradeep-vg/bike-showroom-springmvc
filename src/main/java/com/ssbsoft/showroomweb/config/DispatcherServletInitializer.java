package com.ssbsoft.showroomweb.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

@ComponentScan
public class DispatcherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        System.out.println("-----DispatcherServletInitializer-------getRootConfigClasses---------------");
        return new Class[] {SecurityConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        System.out.println("-----DispatcherServletInitializer-------getServletConfigClasses---------------");
        return null;
    }

    @Override
    protected String[] getServletMappings() {
        System.out.println("-----DispatcherServletInitializer-------getServletMappings---------------");
        return new String[] { "/" };
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        System.out.println("-----DispatcherServletInitializer-------onStartup---------------");
        super.onStartup(servletContext);
        servletContext.addListener(new RequestContextListener());
    }
}