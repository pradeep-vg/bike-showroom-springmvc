<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <title>Product</title>
    <link href="../../assets/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="../../assets/css/mainForm.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="../../assets/css/bootstrap.css">

</head>
<body>
<%@include file="menu.jsp" %>
<div class="page-wrapper bg-gra-01 p-t-180 p-b-100 font-poppins">
    <div class="wrapper wrapper--w780">
        <div class="card card-3" style="width: max-content;margin-left: 25%;">
            <div class="card-body" >
                <c:if test="${error!=null}">
                    <div class="title">
                <c:set var="error" scope="request" value="${error}"/>
                <c:out value="${error}"/>
                    </div>
                </c:if>
                <c:choose>
                    <c:when test="${product.id!=0}">
                        <h2 class="title">Update Bike</h2>
                    </c:when>
                    <c:when test="${product.id==0}">
                        <h2 class="title" style="margin-left: 25%">Add New Bike</h2>
                    </c:when>
                </c:choose>
                <form:form method="post" modelAttribute="product" action="/product">
                    <form:input type="hidden" name="id" value="${product.id}" path="id"/>
                    <form:input type="hidden" name="status" value="ACTIVE" path="status"/>
                    <label class="control-label" style="margin-left: 32%;"><b>BikeName</b></label>
                    <div class="input-group">
                    <form:input type="text"  class="form-control" placeholder="BikeName" name="productName" value="${product.productName == null ? '' :product.productName }" path="productName" required="required"/></div>
                    <label class="control-label" style="margin-left: 33%;"><b>Unit Price</b></label>
                    <div class="input-group">
                    <form:input type="text"  class="form-control"  placeholder="Rupees" name="unitPrice" value="${product.unitPrice == 0 ? '' :product.unitPrice}" path="unitPrice" required="required"/></div>
                    <%-- <label class="control-label" style="margin-left: 38%;"><b>Available Colors</b></label>
                   <div class="input-group">
                       <form:input type="checkbox"  class="form-control" name="color" value="${product.color}" path="color" required="required"/></div>
                  <label class="control-label" style="margin-left: 42%;"><b>Stock</b></label>
                   <div class="input-group">
                   <form:input type="number"  class="form-control"  placeholder="Stock" name="quantity" value="${product.quantity}" path="quantity" required="required" />
                   </div>--%>
                    <label class="control-label" style="margin-left: 42%;"><b>CC</b></label>
                    <div class="input-group">
                        <form:input type="text"  class="form-control"  placeholder="CC" name="CC" value="${product.cc  == 0 ? '' :product.cc}" path="cc" required="required" />
                    </div>
                    <label class="control-label" style="margin-left: 42%;"><b>Mileage</b></label>
                    <div class="input-group">
                        <form:input type="text"  class="form-control"  placeholder="Mileage" name="mileage" value="${product.mileage == 0 ? '' :product.mileage}" path="mileage" required="required" />
                    </div>
                    <label class="control-label" style="margin-left: 42%;"><b>Weight</b></label>
                    <div class="input-group">
                        <form:input type="text"  class="form-control"  placeholder="Weight" name="weight" value="${product.weight == 0 ? '' :product.mileage}" path="weight" required="required" />
                    </div>
                    <c:choose>
                        <c:when test="${product.id!=0}">
                            <button class="btn btn-success" style="margin-left: 35%" type="submit">Update Bike</button>
                        </c:when>
                        <c:when test="${product.id==0}">
                            <button  class="btn btn-success" style="margin-left: 35%" type="submit">Add Bike</button>
                        </c:when>
                        <c:otherwise>
                            "Something Error"
                        </c:otherwise>
                    </c:choose>
                </form:form>
            </div> </div></div></div>
</body>
</html>
