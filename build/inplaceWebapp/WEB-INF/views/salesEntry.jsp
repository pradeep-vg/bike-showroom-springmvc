
<html>
<head>
    <title>Sales Entry</title>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <link rel="stylesheet" type="text/css" href="../../assets/css/table.css">
    <link rel="stylesheet" type="text/css" href="../../assets/css/login.css">
    <link href="../../assets/css/mainForm.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="../../assets/css/bootstrap.css">
</head>
<body>
<%@include file="menu.jsp" %>

<tr>
    <th>Bike Name</th>
    <td><c:out value="${product.productName}"/></td>
</tr>
<tr>
    <th>Unit Price</th>
    <td><c:out value="${product.unitPrice}"/></td>
</tr>
<tr>
    <th colspan="2">Stock</th>
</tr>
<c:if test="${stock == null }">
    <tr><td colspan="2">No Stock Available</td></tr>
</c:if>
<c:forEach var="stock" items="${stock}">
    <tr>
        <td><c:out value="${stock.color.color}"/></td>
        <td><c:out value="${stock.quantity}"/></td>
    </tr>
</c:forEach>

<div class="wrapper wrapper--w780" style="max-width: max-content; margin-top: 5%">
    <div class="card card-3">
        <div class="card-body">
            <h2 class="title" style="margin-left: 7%; width:max-content;">${product.productName} Sales</h2>

            <form:form method="post" modelAttribute="salesDetail" action="/sales">
                <form:input path="product.id" type="hidden" value="${product.id}"/>
                <form:input path="unitPrice" type="hidden" value="${product.unitPrice}"/>

                <label style="color: rgb(55,203,89); font-size: 20px"><b>Color</b></label>
        <form:select path="color.id" required="required" Class="btn btn-info dropdown-toggle" data-toggle="dropdown">
            <c:forEach var="color" items="${stock}" varStatus="counter">
            <option class="dropdown-item" value="${color.color.id}"><c:out value="${color.color.color}"/></option>
            </c:forEach>
        </form:select><br/>
                <label style="color: rgb(55,203,89); font-size: 20px"><b>Customer name</b></label>
                <form:select path="customer.id" required="required" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                    <c:forEach var="customer" items="${customer}" varStatus="counter">
                        <option class="dropdown-item" value="${customer.id}"><c:out value="${customer.username}"/>
                        </option>
                    </c:forEach>
                </form:select><br/>
                    <button type="submit" class="btn btn-success">
                        Confirm to Add
                    </button>
                <a class="btn btn-warning" href="/product">Change Product</a>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>
