<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
    <title>Create user</title>

</head>

<body class="bg">
<%@include file="menu.jsp" %>
<c:choose>
    <c:when test="${user.id==sessionScope.currentUser.id}">
<h1 class="head" style="margin-left:40% ; color: bisque"><c:out value="${sessionScope.currentUser.firstName}"/>&nbsp;Profile</h1>
    </c:when>
    <c:when test="${user.id!=0}">
        <h4>Update User</h4>
    </c:when>
    <c:when test="${user.id==0}">
        <h4>Register New User</h4>
    </c:when>

</c:choose>

<div class="card text-white bg-secondary mb-3" style="width: 50%; margin-left: 25%" >
    <div class="card-header" style="color:sandybrown"><i class="fas fa-table mr-1"></i>${product.productName}Details</div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-dark" id="dataTable" width="100%" cellspacing="0">
                <div class="title" style="margin-left: 34%;">
                ${error}
                </div>
                <form:form method="post" modelAttribute="user" action="/user/createOrUpdate">
                    <div class="form-group">

                        <form:input path="id" type="hidden" name="id" value="${user.id}"/>

                        <label class="control-label col-sm-3" for="firstname">FirstName</label>
                        <div class="col-sm-10">
                            <form:input path="firstName" class="form-control" id="firstname" type="text"
                                        placeholder="FirstName"
                                        name="firstName"
                                        value="${user.firstName}" required="required"/></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="lastname">LastName</label>
                        <div class="col-sm-10">
                            <form:input path="lastName" class="form-control" id="lastname" type="text"
                                        placeholder="LastName"
                                        name="lastName"
                                        value="${user.lastName}" required="required"/></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="mobile">Mobile</label>
                        <div class="col-sm-10">
                            <form:input path="mobile" class="form-control" id="mobile" type="text"
                                        pattern="[6-9]{1}[0-9]{9}"
                                        placeholder="Mobile"
                                        name="mobile" value="${user.mobile}" required="required"/></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Address</label>
                        <div class="col-sm-10">
                            <form:textarea path="address" class="form-control" type="text" placeholder="Address"
                                        name="address"
                                        value="${user.address}" required="required"/></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Email</label>
                        <div class="col-sm-10">
                            <form:input path="email" class="form-control" type="email" placeholder="Email"
                                        name="email"
                                        value="${user.email}" required="required"/></div>
                    </div>
                    <%--<c:choose>
                        <c:forEach var="role" items="${sessionScope.currentUser.roles}" varStatus="counter">
                        <c:when test="${roles.!=user.id&&sessionScope.currentUser.role.id==1}">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="email">Role</label>
                                <div class="col-sm-10">
                                    <form:select path="roles" name="role" class="form-control" required="required">
                                        <c:choose>
                                            <c:when test="${user.roles.equals()}">
                                                <option value="2" selected>Staff</option>
                                                <option value="3">Customer</option>
                                            </c:when>
                                            <c:when test="${user.role.id==3}">
                                                <option value="2">Staff</option>
                                                <option value="3" selected>Customer</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="2">Staff</option>
                                                <option value="3" selected>Customer</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </form:select></div>
                            </div>
                        </c:when>
                        <c:when test="${sessionScope.currentUser.id==user.id}">
                            <form:input path="roles.id" type="hidden" name="role" value="${user.role.id}"/>
                        </c:when>
                        <c:otherwise>
                            <form:input path="roles.id" type="hidden" name="role" value="3"/>
                        </c:otherwise>
                        </c:forEach>
                    </c:choose>--%>

                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">User name</label>
                        <div class="col-sm-10">
                            <form:input path="username"  class="form-control middle qa-new-user-username gl-field-error-outline" placeholder="Username"
                                        name="username"
                                        value="${user.username}" pattern="[a-zA-Z0-9_\.][a-zA-Z0-9_\-\.]*[a-zA-Z0-9_\-]|[a-zA-Z0-9_]" required="required" title="Please create a username with only alphanumeric characters." type="text"/>
                    </div>
                        <c:forEach var="role" items="${sessionScope.currentUser.roles}" varStatus="counter">
                            <c:if test="${(counter.count == 1)&&((role.name.equalsIgnoreCase('admin'))|| (sessionScope.currentUser.id == user.id))}">
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="email">Password</label>
                            <div class="col-sm-10">
                                <form:input path="password" class="form-control" type="password"
                                            placeholder="Password"
                                            name="password" value="${user.getPassword()}" required="required"/></div>
                        </div>
                    </c:if>
                        </c:forEach>
                <div style="margin-left: 40%;">

                <div class="p-t-10">
                        <c:choose>
                            <c:when test="${user.id==sessionScope.currentUser.id}">
                                <button class="btn btn-outline-warning"  type="submit">Update Profile
                                </button>
                            </c:when>

                            <c:when test="${user.id!=0}">
                                <form:input path="status" type="hidden" name= "status" value="ACTIVE"/>
                                <button class="btn btn-outline-warning"  type="submit">Update User
                                </button>
                            </c:when>

                            <c:when test="${user.id==0}">
                                <form:input path="status" type="hidden" name= "status" value="ACTIVE"/>
                                <button class="btn btn-outline-warning" type="submit">Create User
                                </button>
                            </c:when>
                            <c:otherwise>
                                "Something Error"
                            </c:otherwise>
                        </c:choose>
                    </div>
                </form:form>
            </div>
            </div>
        </div>
    </div>
    </div>
</div>
</body>
</html>
