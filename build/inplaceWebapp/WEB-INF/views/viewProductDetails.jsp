<html>
<head>
    <title>Bike Details</title>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%--<link rel="stylesheet" type="text/css" href="../../assets/vendor>
    <link rel="stylesheet" href="../../assets/css/bootstrap.css">--%>
</head>
<body class="bg">
<%@include file="menu.jsp" %>
<h1 class="head" style="margin-left:40% ; color: bisque"><c:out value="${product.productName}"/>&nbsp;Details</h1>
${error}
<a class="btn btn-sm btn-outline-warning" href="/product/update/${product.id}" style="margin-left:77%">Edit details</a>

<div class="card text-white bg-secondary mb-3" style="width: 50%; margin-left: 25%" >
    <div class="card-header" style="color:sandybrown"><i class="fas fa-table mr-1"></i>${product.productName}Details</div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-dark" id="dataTable" width="100%" cellspacing="0">
                <tbody>
                <%-- <tr>
                     <td hidden><c:out value="${product.id}"/></td>
                    </tr>--%>
                <tr>
                    <th>Bike Name</th>
                    <td><c:out value="${product.productName}"/></td>
                </tr>
                <tr>
                    <th>Unit Price</th>
                    <td><c:out value="${product.unitPrice}"/></td>
                </tr>
                <tr>
                    <th colspan="2" style="align-content: center; color:sandybrown">Stock</th>
                </tr>
                <c:if test="${stock == null }">
                    <tr>
                        <td colspan="2">No Stock Available</td>
                    </tr>
                </c:if>
                <c:forEach var="stock" items="${stock}">
                    <tr>
                        <td><c:out value="${stock.color.color}"/></td>
                        <td><c:out value="${stock.quantity}"/></td>
                    </tr>
                </c:forEach>

                <%--<tr>
                    <th>Created By</th>
                    <td><c:out value="${product.createdBy.username}"/></td>
                </tr>
                <tr>
                    <th>Created On</th>
                    <td><c:out value="${product.createdOn}"/></td>
                </tr>
                <tr>
                    <th>Updated By</th>
                    <td aria-placeholder="NA"><c:out value="${product.updatedBy.username == null ? 'N/A' : product.updatedBy.username}"/></td>
                </tr>
                <tr>
                    <th>Updated on</th>
                    <td><c:out value="${product.lastUpdate == null ? 'N/A' : product.lastUpdate }"/></td>
                </tr>--%>
                <tr>
                    <th>CC</th>
                    <td><c:out value="${product.cc}"/>cc</td>
                </tr>
                <tr>
                    <th>Mileage</th>
                    <td><c:out value="${product.mileage}"/>Kmpl</td>
                </tr>
                <tr>
                    <th>Weight</th>
                    <td><c:out value="${product.weight}"/>Kgs</td>
                </tr>
                </tbody>

            </table>
        </div>
    </div>
</div>
</body>
</html>