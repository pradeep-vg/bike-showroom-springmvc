<html>
<head>
    <title>Users</title>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
</head>
<body class="bg">
<%@include file="menu.jsp" %>
<h1 class="site-heading-upper text-primary mb-3" style="margin-left: 50%">CUSTOMERS</h1>
${error}
<a class="btn btn-outline-light" style="margin-left:85%" href="/user/createPage">Add Customer</a>

<div class="card text-white bg-secondary mb-3">
    <div class="card-header"><i class="fas fa-table mr-1"></i>List of staffs</div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-dark" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>S.NO</th>
                    <th hidden>ID</th>
                    <th>FirstName</th>
                    <th>LastName</th>
                    <th>Mobile</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Username</th>
                    <c:if test="${sessionScope.currentUser.id==1}">
                        <th>Password</th>
                    </c:if>
                    <th> Action</th>
                </tr>
                </thead>

                <tbody>
                <c:forEach var="user" items="${customer}" varStatus="counter">
                    <tr>
                        <td><c:out value="${counter.count}"/></td>
                        <td hidden><c:out value="${user.id}"/></td>
                        <td><c:out value="${user.firstName}"/></td>
                        <td><c:out value="${user.lastName}"/></td>
                        <td><c:out value="${user.mobile}"/></td>
                        <td><c:out value="${user.address}"/></td>
                        <td><c:out value="${user.email}"/></td>
                        <td><c:out value="${user.username}"/></td>
                        <c:if test="${sessionScope.currentUser.id==1}">
                            <td><c:out value="${user.password}"/></td>
                        </c:if>
                        <td>

                            <a class="btn btn-sm btn-outline-warning" href="updateUser/${user.id}"><i
                                    class="fas fa-user-edit"></i></a>
                            &nbsp;
                            <a class="btn btn-sm btn-outline-danger" href="deleteUser/${user.id}"><i
                                    class="far fa-trash-alt"></i></a>
                                <%--<form:form method="put" modelAttribute="user" action="/user">
                                    <input type="hidden" name="id" value="${user.id}"/>
                                    <button type="submit"><i class="fas fa-user-edit"></i></button>
                                </form:form>
                                &nbsp;<form:form method="delete" modelAttribute="user" action="/user/${user.id}">
                                <input type="hidden" name="id" value="${user.id}"/>
                                <button type="submit"><i class="far fa-trash-alt fa-2x"></i></button>
                            </form:form>--%>
                        </td>

                    </tr>
                </c:forEach>

                </tbody>

            </table>
        </div>
    </div>
</div>
${error}
<script src="../../assets/vendor/bootstrap/js/all.js"></script>
</body>
</html>
