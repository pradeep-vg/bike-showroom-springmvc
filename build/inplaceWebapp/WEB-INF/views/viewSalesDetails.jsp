<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <title>Sales Details</title>
</head>
<body>
<div class="bg">
    <%@include file="menu.jsp" %>
    <div class="card text-white bg-secondary mb-3" style="width: 50%; margin-left: 25%">
        <div class="card-header" style="color:sandybrown"><i class="fas fa-table mr-1"></i>List of sales Details</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-dark" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>S.NO</th>
                        <th>Customer Name</th>
                        <th>Product Amount</th>
                        <th>Bill Amount</th>
                        <th>Billed Date</th>
                        <%--<th>Billed By</th>--%>
                        <%--<th>Details</th>
                        <th>Cancel</th>--%>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="sales" items="${salesDetails}" varStatus="counter">
                        <tr>
                            <td> ${counter.count}</td>
                                <%--<td hidden><c:out value="${sales.id}"/></td>--%>
                            <td><c:out value="${sales.customer.firstName}"/></td>
                            <td><c:out value="${sales.product.productName}"/></td>
                            <td><c:out value="${sales.product.unitPrice}"/></td>
                            <td><c:out value="${sales.billedOn}"/></td>
                                <%-- <td><c:out value="${sales.billedBy.username}"/></td>--%>
                                <%-- <td>
                                     <a class="btn btn-sm btn-primary"
                                        href="/order/${counter.count}/${order.id}">Details</a>
                                 </td>
                                 <td>
                                     <a class="btn btn-sm btn-outline-danger"
                                        href="/order/deleteOrder/${order.id}">Cancel</a>
                                 </td>--%>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<%--<c:choose>
    <c:when test="${salesDetails!=null}">
        <h1 class="head" style="margin-left:30%; color: bisque">Bill Details for
            bill Number : <c:out value="${sales}"/></h1>
         <table style="margin-top: 1%;margin-left:10%;width:1030px">
            <thead>
            <tr>
                <th>S.NO</th>
                <th>Product Name</th>
                <th>Total Price</th>
                <th>Quantity</th>
                <c:forEach var="salesDetail" items="${salesDetails}" varStatus="counter">
            </thead>
            <tbody>
            <tr>
                <td> ${counter.count}</td>
                <td><c:out value="${salesDetails.product.product.productName}"/></td>
                <td hidden><c:out value="${salesDetails.product.id}"/></td>
                <td><c:out value="${salesDetails.localPrice}"/></td>
                <td><c:out value="${salesDetails.quantity}"/></td>
            </tr>
            </tbody>
            </c:forEach>
        </table>
    </c:when>
</c:choose>--%>
</body>
</html>
