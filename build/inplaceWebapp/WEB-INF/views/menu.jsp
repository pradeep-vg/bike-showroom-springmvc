<%-- <nav style="position: -webkit-sticky"  class="navbar sticky-top navbar-expand-md bg-dark navbar-dark" >
 <div><a class="navbar-brand" href="/home">Yamaha Showroom</a>
 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
 </button></div>
 <div>
 <div class="collapse navbar-collapse" id="collapsibleNavbar">
     <ul class="navbar-nav">
         <c:forEach var="role" items="${sessionScope.currentUser.roles}" >
         <c:if test="${role.name.equalsIgnoreCase('staff')}">
         <li class="nav-item">
             <a class="nav-link" href="/user/customer">Customer</a>
         </li>
          <li class="navbar-nav">
              <a class="nav-link" href="/sales">Sales Details</a>
          </li>
         </c:if>

         <c:if test="${role.name.equalsIgnoreCase('admin')}">
         <li class="nav-item">
             <a class="nav-link" href="/user/staff">Staffs</a>
         </li>
         </c:if>

             <c:if test="${role.name.equalsIgnoreCase('customer')}">
         <li class="nav-item">
             <a class="nav-link" href="/product">Bikes</a>
         </li>

         <li class="nav-item">
             <a class="nav-link" href="/product">My Bikes</a>
         </li>
             </c:if>
         </c:forEach>

         </ul>
 </div>
 </div>
     <div style="margin-inline-start: 50%;">
         <ul class="navbar-nav mr-auto mt-2 mt-lg-0 justify-content-end">
             <li class="nav-item" style="margin-right:2%;">
                 <a class="btn btn-success"  href="/user/updateUser/${sessionScope.currentUser.id}">Profile</a>
             </li>

             <li class="nav-item ">
                 <a class="btn btn-danger"  href="/logout">Logout</a>
             </li>
         </ul>
     </div>
</nav>--%>
<html>
<head>
    <!-- Bootstrap core CSS -->
    <link href="../../assets/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../../assets/vendor/fontawesome/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../assets/vendor/bootstrap/css/business-casual.css" rel="stylesheet">

</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
    <div class="container">
        <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none"
           href="#">${sessionScope.currentUser.firstName}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav mx-auto">

                <li class="nav-item active px-lg-4">
                    <a class="nav-link text-uppercase text-expanded" href="/home"><i class="fas fa-home"></i>&nbsp;Home
                        <%--<span class="sr-only">(current)</span>--%>
                    </a>
                </li>


                <li class="nav-item active px-lg-4">
                    <a class="nav-link text-uppercase text-expanded" href="/product"><i class="fas fa-motorcycle"></i>&nbsp;Bikes</a>
                </li>
                <
                <c:forEach var="role" items="${sessionScope.currentUser.roles}" varStatus="counter">
                <c:if test="${(counter.count == 1)&&(role.name.equalsIgnoreCase('admin')||role.name.equalsIgnoreCase('staff'))}">
                <li class="nav-item active px-lg-4">
                    <a class="nav-link text-uppercase text-expanded" href="/sales"><i class="fas fa-th-list"></i>&nbsp;Sales Details</a>
                </li>
                </c:if>

                <%--<c:if test="${role.name.equalsIgnoreCase('customer')}">
                <li class="nav-item px-lg-4">
                    <a class="nav-link text-uppercase text-expanded" href="/products">My bike</a>
                </li>
                </c:if>--%>

                <c:if test="${(counter.count == 1)&&(role.name.equalsIgnoreCase('admin')||role.name.equalsIgnoreCase('staff'))}">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item active dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                           data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-users"></i>&nbsp;USERS</a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                            <c:if test="${(counter.count == 1)&&(role.name.equalsIgnoreCase('admin'))}">

                                <a class="dropdown-item" href="/user/staff"><i class="fas fa-user-tie"></i>&nbsp;Staffs</a>

                            </c:if>
                            <c:if test="${(counter.count == 1)&&(role.name.equalsIgnoreCase('admin') || role.name.equalsIgnoreCase('staff'))}">

                                <a class="dropdown-item" href="/user/customer"><i class="fas fa-user-tag"></i>&nbsp;Customers</a>

                            </c:if>
                            <div class="dropdown-divider"></div>

                            <a class="dropdown-item" href="/user/createPage"><i class="fas fa-user-plus"></i>&nbsp;Add User</a>

                            </c:if>
                        </div>
                    </li>
                </ul>

                </c:forEach>

                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                           data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false"><i class="fas fa-user-circle fa-2x"></i></a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">${sessionScope.currentUser.username}</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/user/updateUser/${sessionScope.currentUser.id}">
                                <i class="fas fa-user"></i>&nbsp;Profile</a>
                            <a class="dropdown-item" href="/logout"><i class="fas fa-sign-out-alt"></i> Logout</a>
                        </div>
                    </li>
                </ul>
        </div>
    </div>
</nav>
<!-- Bootstrap core JavaScript -->

<script src="../../assets/vendor/jquery/jquery.min.js"></script>
<script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>

